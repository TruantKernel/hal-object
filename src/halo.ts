import { HalHyperlink } from "./link-object";

export type HalLinks = {
  [rel: string]: HalHyperlink[];
};

export class Halo<T> {
  public static readonly EMBEDDED_PROPERTY_NAME = "_embedded";
  public static readonly LINKS_PROPERTY_NAME = "_links";
  public static readonly SELF_RELATION = "self";
  protected _embedded?: { [rel: string]: Halo<any>[] };
  protected _links?: HalLinks;
  protected data: T;

  public constructor(json: unknown) {
    this._links = this.registerLinks(json);
    this._embedded = this.registerEmbedded(json);
    this.data = this.registerProperties(json);
  }

  protected registerEmbedded(json: any): { [rel: string]: Halo<any>[] } {
    const embedded = {};
    if (!json._embedded) {
      return embedded;
    }

    for (const rel of Object.keys(json._embedded)) {
      const related = json._embedded[rel];
      if (Array.isArray(related)) {
        embedded[rel] = new Array<Halo<any>>();
        for (let i = 0; i < related.length; i++) {
          (embedded[rel] as Halo<any>[])[i] = new Halo(related[i]);
        }
      } else {
        embedded[rel] = new Array<Halo<any>>(new Halo(related));
      }
    }
    
    return embedded;
  }

  protected registerProperties(json: any): T {
    const data: { [rel: string]: any } = {};
    for (const entry of Object.entries(json)) {
      const prop: string = entry[0] as string;
      if (!(prop as string).startsWith("_")) {
        data[prop] = entry[1];
      }
    }
    return data as T;
  }

  protected registerLinks(json: any): HalLinks {
    const links: HalLinks = {};
    if (!json._links) {
      return links;
    }
    for (const rel of Object.keys(json._links)) {
      const related = json._links[rel];
      links[rel] = !Array.isArray(related)
        ? new Array<HalHyperlink>(related)
        : related;
    }
    return links;
  }

  public getData(): T {
    return this.data;
  }

  public getEmbeddedOfRelation(rel: string): Halo<any>[] | undefined {
    return (this._embedded as { [rel: string]: Halo<any>[] })[rel];
  }

  public getEmbedded(): { [rel: string]: Halo<any>[] } {
    return (this._embedded as { [rel: string]: Halo<any>[] });
  }

  public getLinkRelations(): Array<string> {
    const relations = new Array<string>();
    for (const rel of Object.keys(this._links as HalLinks)) {
      relations.push(rel);
    }
    return relations;
  }

  public getEmbeddedRelations(): Array<string> {
    const relations = new Array<string>();
    if (!this._embedded) {
      return relations;
    }
    for (const rel of Object.keys(this._embedded)) {
      relations.push(rel);
    }
    return relations;
  }

  public getLinks(): HalLinks {
    return (this._links as HalLinks);
  }

  public getLinksOfRelation(rel: string): HalHyperlink[] | undefined {
    return (this._links as HalLinks)[rel];
  }

  public getSelfLink(): HalHyperlink | undefined {
    const self = this.getLinksOfRelation(Halo.SELF_RELATION) as HalHyperlink[];
    return self ? self[0] : undefined;
  }
}
