export interface HalHyperlink {
  href: string;
  templated: boolean;
  type: string;
  depreciation: string;
  name: string;
  profile: string;
  title: string;
  hreflang: string;
}
