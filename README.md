# HAL Object
`hal-object` is a simple implementation of [JSON Hypertext Application Language](https://datatracker.ietf.org/doc/html/draft-kelly-json-hal), written in Typescript, for the purpose of consuming JSON from external APIs.

This library is utilized by its developer but has not been extensively tested in production. Please use with caution.

# Examples

Take an example JSON

```javascript
{
  _links: {
    self: {
      href: "http://example.org/api/foo",
    },
    root: {
      href: "http://example.org/api",
    },
    collection: [
      { href: "http://example.org/one" },
      { href: "http://example.org/two" },
    ],
  },
  id: "foo",
  property: "bar",
  _embedded: {
    related: [
      {
        _links: {
          self: {
            href: "http://example.org/api/biz",
          },
        },
        id: "biz",
        property: "baz",
      },
      {
        _links: {
          self: {
            href: "http://example.org/api/qux",
          },
        },
        id: "qux",
        name: "lok",
      },
    ],
    additional: {
      _links: {
        self: {
          href: "http://example.org/api/foobar",
        },
      },
      id: "foobar",
      property: "barfoo",
    },
  },
}
```
Instantiate HAL object

```javascript
let halo = new HALO(json)
```

## Retrieve `state` data of JSON

```javascript
halo.getData();
```
 returns
 
 ```javascript
 { id: "foo", property: "bar" }
 ```
 
## Retrieve links

### self

```javascript
halo.getSelfLink();
```

 returns
 
 ```javascript
{ href: "http://example.org/api/foo", }
 ```
  
### other links

```javascript
(halo.getLinks("collection") as LinkObject[])[0];
```
 
 returns
 
 ```javascript
{ href: "http://example.org/one", }
```

## Retrieve a list of link relations

```javascript
halo.getLinkRelations();
```

returns an array of link relations

```javascript
[
	"self",
	"root",
	"collection",
]
```

## Retrieve embedded

```javascript
(halo.getEmbedded("additional") as HALO[])[0];
```

returns the embedded HAL Object

```javascript
{
	_links: { self: { href: "http://example.org/api/foobar" } },
	id: "foobar",
	property: "barfoo",
}
```

## Retrieve a list of embedded relations

```javascript
halo.getEmbeddedRelations();
```

returns an array of embedded relations

```javascript
[
	"related",
	"additional",
]
```

# Extensions

#### [cacheable-hal-object](https://gitlab.com/073TruantKernel/cacheable-hal-object)
A library that extends `hal-object` to add caching information

#### [tealeaf-object](https://gitlab.com/073TruantKernel/tealeaf-object)
An experimental library that extends `cacheable-hal-object` to add a property for BEM related data.
 

 