import { HalHyperlink, Halo } from "../src";
import { valid, noSelf } from "./__fixtures__/fixtures";
let halo: Halo<any>;
let halo2: Halo<any>; // this game was the best.
let empty: Halo<any>;

describe("Halo class", function () {
  test("empty Halo instantiable", () => {
    expect((empty = new Halo<any>({})));
  });

  test("partial Halo instantiable", () => {
    expect((halo2 = new Halo<any>(noSelf)));
  });

  test("Halo instantiable", () => {
    expect((halo = new Halo<any>(valid)));
  });

  test("root Halo<any> contains data", () => {
    expect(halo.getData()).toStrictEqual({ id: "foo", property: "bar" });
  });

  test("root Halo contains link to self", () => {
    expect((halo.getLinksOfRelation("self") as HalHyperlink[])[0]).toStrictEqual({
      href: "http://example.org/api/foo",
    });
  });

  test("Halo can get link to self with getSelfLink", () => {
    expect(halo.getSelfLink()).toStrictEqual({
      href: "http://example.org/api/foo",
    });
  });

  test("Halo without self link returns undefined from getSelfLink", () => {
    expect(halo2.getSelfLink()).toBeUndefined();
  });

  test("Halo can get all link relations", () => {
    expect(halo.getLinkRelations()).toStrictEqual([
      "self",
      "root",
      "collection",
    ]);
    expect(empty.getLinkRelations()).toHaveLength(0);
  });

  test("Halo can get all embedded relations", () => {
    expect(halo.getEmbeddedRelations()).toStrictEqual([
      "related",
      "additional",
    ]);
    expect(empty.getEmbeddedRelations()).toHaveLength(0);
  });

  test("Halo can get all embedded Halos", () => {
    expect(
      (halo.getEmbeddedOfRelation("additional") as Halo<any>[])[0]
    ).toStrictEqual(
      new Halo<any>({
        _links: { self: { href: "http://example.org/api/foobar" } },
        id: "foobar",
        property: "barfoo",
      })
    );
  });

  test("Halo can get specified embedded Halo", () => {
    expect(halo.getEmbedded()).toStrictEqual({
      additional: [
        new Halo<any>({
          _links: { self: { href: "http://example.org/api/foobar" } },
          id: "foobar",
          property: "barfoo",
        }),
      ],
      related: [
        new Halo<any>({
          _links: {
            self: { href: "http://example.org/api/biz" },
          },
          id: "biz",
          property: "baz",
        }),
        new Halo<any>({
          _links: {
            self: { href: "http://example.org/api/qux" },
          },
          id: "qux",
          name: "lok",
        }),
      ],
    });
  });
});
